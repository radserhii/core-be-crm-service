<?php

namespace App\Services;

use GuzzleHttp\Client;
use GuzzleHttp\RequestOptions;

class HttpFormBuilderService
{
    private $client;

    public function __construct()
    {
        $this->client = new Client([
            'base_uri' => env('FORM_BUILDER_API_URL'),
            'query' => ['api_token' => env('FORM_BUILDER_API_TOKEN')],
        ]);
    }

    public function getForms($request)
    {
        $response = $this->client->get('/api/v1/forms', [RequestOptions::JSON => $request]);
        return $response->getBody()->getContents();
    }

    public function getFormById($id)
    {
        $response = $this->client->get("/api/v1/forms/{$id}");
        return $response->getBody()->getContents();
    }

    public function createForm($form)
    {
        $response = $this->client->put("/api/v1/forms", [RequestOptions::JSON => $form]);
        return $response->getBody()->getContents();
    }

    public function updateForm($form, $id)
    {
        $response = $this->client->post("/api/v1/forms/{$id}", [RequestOptions::JSON => $form]);
        return $response->getBody()->getContents();
    }

    public function deleteForm($id)
    {
        $response = $this->client->delete("/api/v1/forms/{$id}");
        return $response->getBody()->getContents();
    }

    public function bulkSoftDeleteForm($ids)
    {
        $response = $this->client->delete("/api/v1/forms", [
            'json' => ['id' => $ids]
        ]);
        return $response->getBody()->getContents();
    }

    public function bulkSetStatusForm($ids, $status)
    {
        $response = $this->client->post(
            "/api/v1/forms/status",
            [RequestOptions::JSON => ['id' => $ids, 'status' => $status]]);
        return $response->getBody()->getContents();
    }

    public function getSchema()
    {
        $response = $this->client->get('/api/v1/schema');
        return $response->getBody()->getContents();
    }

    public function getMapped()
    {
        $response = $this->client->get('/api/v1/mapped');
        return $response->getBody()->getContents();
    }

    public function getFormsByPersonId($id)
    {
        $response = $this->client->get("/api/v1/forms/person/{$id}");
        return $response->getBody()->getContents();
    }
}
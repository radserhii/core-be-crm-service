<?php

namespace App\Models;

use App\Traits\ListTrait;
use Illuminate\Auth\Authenticatable;
use Laravel\Lumen\Auth\Authorizable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Notifications\Notifiable;


class User extends Model implements AuthenticatableContract, AuthorizableContract
{
    use Authenticatable, Authorizable, ListTrait, Notifiable;

    protected $table = "users";
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id', 'username', 'email', 'password', 'full_name', 'role',
        'admin', 'state', 'city', 'address', 'zip', 'phone', 'status'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'password_reset_token'
    ];

    protected $with = ['role'];

    public function role()
    {
        return $this->hasOne('App\Models\Roles', 'id', 'role');
    }

    static public function rules($id = NULL){
        return [
            'username' => 'required|unique:users,username,' . $id,
            'password' => 'required',
            'email' => 'required|email|unique:users,email,' . $id,
        ];
    }

    static public function updateRules($id = NULL) {
        return [
            'username' => 'required|unique:users,username,' . $id,
            'email' => 'required|email|unique:users,email,' . $id,
        ];
    }

    static public function authorizeRules() {
        return [
            'username' => 'required',
            'password' => 'required',
        ];
    }

    public function routeNotificationForMail() {
        return $this->email;
    }

    public function getList($params) {
        $count = $this->getListQuery($params, false)->count();
        $query = $this->getListQuery($params);
        return [
            'data' => $query->get(),
            'page' => isset($params['page']) ? $params['page'] : 1,
            'total' => $count
        ];
    }

    public static function authorize($attributes) {

        $model = User::where(['username' => $attributes['username']])
            ->select(['id', 'username', 'password', 'status', 'role', 'email', 'full_name'])
            ->first();
        if (!$model) {
            return false;
        }
        if ($model->status != 'active') {
            return false;
        }
        if (Hash::check($attributes['password'], $model->password)) {
            return $model;
        }
        return false;
    }

    public function getDistributorsList($params) {
        $model = $this->getDistributorQuery();
        $count = $this->getRelationQuery($model, $params, false)->count();
        $query = $this->getRelationQuery($model, $params, true);
        return [
            'data' => $query->get(),
            'page' => isset($params['page']) ? $params['page'] : 1,
            'total' => $count,
        ];
    }


    public static function getDistributorQuery() {
        return DB::table('users')
            ->leftJoin('roles', 'roles.id', '=', 'users.role')
            ->select('users.id', 'username', 'email', 'full_name', 'admin', 'state', 'city', 'address', 'zip', 'phone',
                'users.role', 'users.created_at', 'users.status', 'users.updated_at', 'users.out_of_town');
    }

}

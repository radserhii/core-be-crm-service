<?php
namespace App\Traits;
use Carbon\Carbon;

trait ListTrait
{
    function getListQuery($params, $applyLimit = true)
    {
        $limit = $params['limit'];
        $page = $params['page'];
        $order = $params['sort'];
        $limit = isset($limit) ? $limit : 10;
        $page = isset($page) ? $page : 1;
        $offset = ($page - 1) * $limit;
        $query = $this->select($this->fillable);
        if ($applyLimit) {
            $query->limit($limit);
            $query->offset($offset);
        }
        if (isset($params['filter'])) {
            foreach ($params['filter'] as $key => $value) {
                if (!in_array($key, $this->fillable)) {
                    continue;
                }
                $query->where([$key => $value]);
            }
        }
        if (isset($params['search'])) {
            foreach ($params['search'] as $key => $value) {
                if (!in_array($key, $this->fillable)) {
                    continue;
                }
                $query->where($key, 'like', $value.'%');
            }
        }

        if (isset($params['conditions'])) {
            foreach ($params['conditions'] as $key => $value) {
                if (!$this->checkFieldExist($key)) {
                    continue;
                }
                $field = $key;//$this->getColumnName($key);
                if (is_string($value) || is_numeric($value)) {
                    $query->where($field, $value);
                } else if (is_array($value) && isset($value['or'])) {
                    $query->whereIn($field, $value['or']);
                } else if (is_array($value) && isset($value['from']) && isset($value['to'])) {
                    $from = $this->formatDate($value['from'], true);
                    $to = $this->formatDate($value['to']);
                    $query->where($field, '>=', $from);
                    $query->where($field, '<=', $to);
                }
            }
        }
        if (isset($order) && isset($order['field']) && isset($order['order'])) {
            $query->orderBy($order['field'], $order['order']);
        }
        return $query;
    }

    //XXX too large
    function getRelationQuery($query, $params, $applyLimit = true) {
        // Table name is needed to avoid fields name conflicts
        if (!is_null($this->table)) {
            $table = $this->table.'.';
        } else {
            $table = '';
        }
        $limit = $params['limit'];
        $page = $params['page'];
        if (isset($params['sort']) && isset($params['sort']['field']) && $this->checkFieldExist($params['sort']['field'])) {
            $params['sort']['field'] = $this->getColumnName($params['sort']['field']);
            $order = $params['sort'];
            if (isset($order) && isset($order['field']) && isset($order['order'])) {
                $query->orderBy($order['field'], $order['order']);
            }
        }

        $limit = isset($limit) ? $limit : 10;
        $page = isset($page) ? $page : 1;
        $offset = ($page - 1) * $limit;
        if ($applyLimit) {
            $query->limit($limit);
            $query->offset($offset);
        }

        if (isset($params['filter'])) {
            foreach ($params['filter'] as $key => $value) {
                if (!$this->checkFieldExist($key)) {
                    continue;
                }
                $query->where($this->getColumnName($key), '=', $value);
            }
        }

        if (isset($params['search'])) {
            foreach ($params['search'] as $key => $value) {
                if (!$this->checkFieldExist($key)) {
                    continue;
                }
                $query->where($this->getColumnName($key), 'like', $value.'%');
            }
        }

        if (isset($params['conditions'])) {
            foreach ($params['conditions'] as $key => $value) {
                if (!$this->checkFieldExist($key)) {
                    continue;
                }
                $field = $this->getColumnName($key);
                if (is_string($value) || is_numeric($value)) {
                    $query->where($field, $value);
                } else if (is_array($value) && isset($value['or'])) {
                    $this->queryForOr($query, $value, $field);
                } else if (is_array($value) && isset($value['from']) && isset($value['to'])) {
                    $from = $this->formatDate($value['from'], true);
                    $to = $this->formatDate($value['to']);
                    $query->where($field, '>=', $from);
                    $query->where($field, '<=', $to);
                }
            }
        }

        return $query;
    }

    public function queryForOr($query, $value, $field) {
        if (is_array($value['or'])) {
            $query->where(function ($queries) use ($value, $field){
                foreach ($value['or'] as $value_or) {
                    if (is_array($value_or) && isset($value_or['from']) && isset($value_or['to'])) {
                        $queries->orWhere(function ($quer) use ($value_or, $field) {
                            $quer->where($field, '>=', $value_or['from']);
                            $quer->where($field, '<=', $value_or['to']);
                        });
                    } else {
                        $queries->orWhere($field, $value_or);
                    }
                }
            });
        } else {
            $query->whereIn($field, $value['or']);
        }
        if ($field  == 'delivers.delivery_price') {
            $itemPriceField = 'invoices.item_price';
            if (is_array($value['or'])) {
                $query->orWhere(function ($queries) use ($value, $itemPriceField){
                    foreach ($value['or'] as $value_or) {
                        if (is_array($value_or) && isset($value_or['from']) && isset($value_or['to'])) {
                            $queries->orWhere(function ($quer) use ($value_or, $itemPriceField) {
                                $quer->where($itemPriceField, '>=', $value_or['from']);
                                $quer->where($itemPriceField, '<=', $value_or['to']);
                            });
                        } else {
                            $queries->orWhere($itemPriceField, $value_or);
                        }
                    }
                });
            } else {
                $query->orWhere(function ($queries) use ($value, $itemPriceField){
                    $queries->whereIn($itemPriceField, $value['or']);
                });
            }
        }
    }

    public function getColumnName($key) {
        if(array_key_exists($key, $this->joinSearch)) {
            $field = $this->joinSearch[$key];
            if (strpos($field, '.') !== false) {
                return $field;
            } else {
                return $field.'.'.$key;
            }
        } else {
            return $this->table.'.'.$key;
        }
    }

    public function formatDate($str, $isFrom = false) {
        $reg = "/[0-9]{4}-[0-9]{2}-[0-9]{2}/";
        if (!preg_match($reg, $str)) {
            return $str;
        }
        $date = Carbon::createFromFormat('Y-m-d', $str);
        return $isFrom ? $date->startOfDay() : $date->endOfDay();
    }

    public function checkFieldExist($key) {
        if (isset($this->joinSearch[$key])) {
            return true;
        }
        if (in_array($key, $this->fillable)) {
            return true;
        }
        return false;
    }

}
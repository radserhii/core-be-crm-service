<?php

namespace App\Providers;

use App\Models\User;
use App\Models\AccessTokens;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\ServiceProvider;
use Illuminate\Http\Request;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        // Here you may define how you wish users to be authenticated for your Lumen
        // application. The callback which receives the incoming request instance
        // should return either a User instance or null. You're free to obtain
        // the User instance via an API token or any other method necessary.

        $this->app['auth']->viaRequest('api', function (Request $request) {
            if (!isset($_SERVER['HTTP_HOST'])
                || (!isset($_SERVER['HTTP_X_ACCESS_TOKEN']) && !isset($_GET['token']))) {
                return null;
            }
            $token = isset($_SERVER['HTTP_X_ACCESS_TOKEN']) ? $_SERVER['HTTP_X_ACCESS_TOKEN'] : $_GET['token'];
            if($token) {
                return $this->findIdentityByAccessToken($token);
            }
            return null;
        });
    }

    /**
     * Boot the authentication services for the application.
     *
     * @return void
     */
    public function boot()
    {

    }
    public function findIdentityByAccessToken($token)
    {
        $access_token = AccessTokens::where(['token' => $token])->first();

        if ($access_token) {
            if ($access_token->expires_at < time()) {
                return null;
            }
            return User::where(['id'=>$access_token->user_id])->first();

        }
        return null;
    }
}

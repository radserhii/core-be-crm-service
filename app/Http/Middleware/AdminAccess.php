<?php

namespace App\Http\Middleware;
use Closure;
use Illuminate\Support\Facades\Auth;


class AdminAccess
{
    public function handle($request, Closure $next) {
        if (Auth::user() && Auth::user()->getAttributes()['admin']) {
            return $next($request);
        }
        abort(401, 'Not authenticated');
    }
}
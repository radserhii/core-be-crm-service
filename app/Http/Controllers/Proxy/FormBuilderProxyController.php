<?php

namespace App\Http\Controllers\Proxy;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Services\HttpFormBuilderService;
use Illuminate\Support\Facades\Auth;

class FormBuilderProxyController extends Controller
{
    private $httpFormBuilderService;

    public function __construct(HttpFormBuilderService $httpFormBuilderService)
    {
        $this->httpFormBuilderService = $httpFormBuilderService;
    }

    public function get(Request $request)
    {
        $params = json_decode($request->get('params'));
        return response($this->httpFormBuilderService->getForms($params));
    }

    public function getById($id)
    {
        return response($this->httpFormBuilderService->getFormById($id));
    }

    public function create(Request $request)
    {
        $ownerId = Auth::id();
        $form = $request->all();
        $form['owner_id'] = $ownerId;
        return response($this->httpFormBuilderService->createForm($form));
    }

    public function update(Request $request, $id)
    {
        return response($this->httpFormBuilderService->updateForm($request->all(), $id));
    }

    public function delete($id)
    {
        return response($this->httpFormBuilderService->deleteForm($id));
    }

    public function bulkSoftDelete(Request $request)
    {
        $ids = $request->get('id');
        return response($this->httpFormBuilderService->bulkSoftDeleteForm($ids));
    }

    public function bulkSetStatus(Request $request)
    {
        $ids = $request->get('id');
        $status = $request->get('status');
        return response($this->httpFormBuilderService->bulkSetStatusForm($ids, $status));
    }

    public function schema()
    {
        return response($this->httpFormBuilderService->getSchema());
    }

    public function mapped()
    {
        return response($this->httpFormBuilderService->getMapped());
    }

    public function getFormsByPersonId($id)
    {
        return response($this->httpFormBuilderService->getFormsByPersonId($id));
    }
}

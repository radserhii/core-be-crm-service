<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\LoginRequest;
use Illuminate\Support\Facades\{
    Auth, Hash
};
use App\Models\{
    User, Roles, AccessTokens, Log};
use \Edminify\Skeleton\PermissionsHelper;

class UserController extends Controller
{
    public function __construct()
    {
        $this->middleware('admin', [
            'only' => ['deleteRecord', 'createPersonUser', 'createOrganisationUser', 'createAdminUser', 'users']
        ]);
    }

    public function login(LoginRequest $request)
    {
        if ($model = User::authorize($request->all())) {
            $accessToken = $this->createAccesstoken($model->id);
            $data = [];
            $data['access_token'] = $accessToken->token;
            $data['expires_at'] = $accessToken->expires_at;
            $data['username'] = $model->username;
            $data['email'] = $model->email;
            $data['full_name'] = $model->full_name;
            $data['role'] = Roles::where(["id" => $model->role])->first()->role_name;
            $data['permissions'] = PermissionsHelper::getPermission($model->id);
            Log::info($model->username . ' was logged in', $model->id, $request->ip());
            return $this->sendResponse($data);
        } else {
            return $this->sendError(422, 'Username or Password is incorrect or your account is blocked');
        }
    }

    public function createAdminUser(Request $request)
    {
        $this->validate($request, User::rules());
        $attributes = $request->all();
        $attributes['admin'] = 1;
        $attributes['role'] = 1;
        $attributes['status'] = 'active';
        $attributes['password'] = Hash::make($attributes['password']);
        $model = User::create($attributes);
        Log::crudMessage('create', 'Admin User',
            Auth::user()->getAttributes(), $model->username, $request->ip());
        return $this->sendResponse($model);
    }

    public function createOrganisationUser(Request $request)
    {
        $this->validate($request, User::rules());
        $attributes = $request->all();
        $attributes['admin'] = 1;
        $attributes['role'] = 2;
        $attributes['status'] = 'active';
        $attributes['password'] = Hash::make($attributes['password']);
        $model = User::create($attributes);
        Log::crudMessage('create', 'Organisation User',
            Auth::user()->getAttributes(), $model->username, $request->ip());
        //AdminNotifications::notifyAdmin(new DistributorAdd($model));
        return $this->sendResponse($model);
    }

    public function createPersonUser(Request $request)
    {
        $this->validate($request, User::rules());
        $attributes = $request->all();
        $attributes['admin'] = 1;
        $attributes['role'] = 3;
        $attributes['status'] = 'active';
        $attributes['password'] = Hash::make($attributes['password']);
        $model = User::create($attributes);
        Log::crudMessage('create', 'Person User',
            Auth::user()->getAttributes(), $model->username, $request->ip());
        //AdminNotifications::notifyAdmin(new DistributorAdd($model));
        return $this->sendResponse($model);
    }

    public function users(Request $request)
    {
        $params = [
            'sort' => $request->input('sort'),
            'limit' => $request->input('limit'),
            'page' => $request->input('page'),
            'search' => $request->input('search'),
            'conditions' => $request->input('conditions')
        ];
        $filter = $request->input('filter');
        if (isset($filter)) {
            $params['filter'] = $filter;
        }
        $model = new User();
        $response = $model->getList($params);
        return $this->sendListResponse($response);
    }

    public function me()
    {
        /** @var $user User */
        $user = Auth::user();
        $user->makeHidden(['created_at', 'updated_at']);
        $user['permissions'] = PermissionsHelper::getPermission($user->id);
        return $this->sendResponse($user);
    }


    public function refresh(Request $request)
    {
        $headers = $request->headers->all();
        if (!$access_token = $this->refreshAccesstoken($headers['x-access-token'])) {
            return $this->sendError(401, 'Invalid Access token');
        }
        $data = [];
        $data['access_token'] = $access_token->token;
        $data['expires_at'] = $access_token->expires_at;
        return $this->sendResponse($data);
    }

    public function logout(Request $request)
    {
        $token = $this->getAccessToken($request);
        $model = AccessTokens::where(['token' => $token])->first();
        if ($model != null) {
            if ($model->delete()) {
                $response = [
                    'status' => 1,
                    'message' => "Logged Out Successfully"
                ];
                $user = Auth::user()->getAttributes();
                Log::info($user['username'] . ' was logged out', $user['id'], $request->ip());
                return response()->json($response, 200, [], JSON_PRETTY_PRINT);
            } else {
                return $this->sendError(400, 'Invalid request');
            }
        } else {
            return $this->sendError(400, 'Invalid request');
        }
    }


    public function view($id)
    {
        $user = Auth::user()->getAttributes();
        if ($user['id'] !== $id && !$user['admin']) {
            return $this->sendError(401, 'Access Denied');
        }
        $model = $this->findModel($id);
        return response()->json($model, 200, [], JSON_PRETTY_PRINT);
    }

    public function update(Request $request, $id)
    {
        $user = Auth::user()->getAttributes();
        if ($user['id'] !== $id && !$user['admin']) {
            return $this->sendError(401, 'Access Denied');
        }
        $model = $this->findModel($id);
        $attributes = $request->all();
        $this->validate($request, User::updateRules($id));
        if (!empty($attributes['password'])) {
            $model->password = Hash::make($attributes['password']);
            $accessToken = AccessTokens::where(['user_id' => $id])->first();
            if (isset($accessToken)) {
                $accessToken->delete();
            }
            unset($attributes['password']);
        }
        $model->fill($attributes);
        $model->save();
        Log::crudMessage('update', 'User', Auth::user()->getAttributes(), $model->username, $request->ip());
        return $this->sendResponse($model);
    }

    public function deleteRecord(Request $request, $id)
    {
        $model = $this->findModel($id);
        $model->delete();
        $response = [
            'status' => 1,
            'data' => $model,
            'message' => 'Removed successfully.'
        ];
        Log::crudMessage('delete', 'User', Auth::user()->getAttributes(), $model->username, $request->ip());
        return response()->json($response, 200, [], JSON_PRETTY_PRINT);
    }

    public function allDistributors(Request $request)
    {
        $user = Auth::user()->getAttributes();
        if (!$user['admin']) {
            $response = User::where('id', '=', $user['id'])->get();
        } else {
            $response = User::where('admin', '<>', 1)->get();
        }
        return $this->sendResponse($response);
    }

    public function findModel($id)
    {
        $model = User::where('id', $id)
            ->where('status', 'active')
            ->first();
        if (!$model) {
            return $this->sendError(404, 'User not found or blocked');
        }
        return $model;
    }

    public function createAccesstoken($userId)
    {
        $model = new AccessTokens();
        $model->token = md5(uniqid());
        $model->expires_at = time() + (60 * 60 * 24 * 60); // 60 days
        $model->user_id = $userId;
        $model->created_at = time();
        $model->updated_at = time();
        $model->save();
        return $model;
    }

    public function refreshAccesstoken($token)
    {
        $access_token = AccessTokens::where(['token' => $token])->first();
        if ($access_token) {
            $access_token->delete();
            $new_access_token = $this->createAccesstoken($access_token->user_id);
            return ($new_access_token);
        } else {
            return false;
        }
    }

    public function getAccessToken($request)
    {
        $headers = $request->headers->all();
        $token = false;
        if (!empty($headers['x-access-token'][0])) {
            $token = $headers['x-access-token'][0];
        } else if ($request->input('access_token')) {
            $token = $request->input('access_token');
        }
        return $token;
    }

    public function searchUsers(Request $request)
    {
        $search = $request->input('search');
        if (!$search) {
            return $this->sendResponse([]);
        }
        $data = User::where('full_name', 'like', $search . '%')
            ->select('id', 'username', 'full_name', 'created_at', 'email')
            ->get();
        return $this->sendResponse($data);
    }


    public function distributorView(Request $request, $id)
    {
        $query = User::getDistributorQuery();
        $query->where('users.id', $id);
        return $this->sendResponse($query->first());
    }

    public function statusChange(Request $request, $id)
    {
        $user = Auth::user()->getAttributes();
        $status = $request->input('status');
        if ($user['id'] == $id) {
            return $this->sendError(422, 'You can not change status of current user');
        }
        $model = User::find($id);
        if (is_null($model)) {
            return $this->sendError(422, 'User not found');
        }
        $model->status = $status == 'inactive' ? 'inactive' : 'active';
        $model->save();
        /*if ($status == 'inactive') {
            AccessTokens::where('user_id', $id)->delete();
            Store::where('user_id', $id)->update(['status' => 'inactive']);
        } else {
            Store::where('user_id', $id)->update(['status' => 'active']);
        }*/
        return $this->sendResponse(['status' => 'ok']);
    }
}

?>
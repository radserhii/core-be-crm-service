<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Person;

class PersonController extends Controller
{
    const DEFAULT_LIMIT = 15;

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function create(Request $request)
    {
        if (!$this->hasAccess($request, "person", 0, 1)) {
            return $this->sendError(401, 'Access denied');
        }
            $form = $request->post();

            if (empty($form)) {
                throw new \Exception('No input data!');
            }

            $new = new Person();
            $new->fill($form);

            return response()
                ->json([
                    'created' => $new->save(),
                    'id' => $new->getAttribute('id'),
                ]);

    }

    /**
     * @param Request $request
     * @return \Illuminate\Database\Eloquent\Builder[]|\Illuminate\Database\Eloquent\Collection
     */
    public function search(Request $request)
    {
        $offset = $request->get('offset') ?? 0;
        $limit = $request->get('limit') ?? self::DEFAULT_LIMIT;

        $order = $request->get('order') ?? 'id';
        $direction = $request->get('direction') ?? 'ASC';

        return Person::query()
            ->orderBy($order, $direction)
            ->offset($offset)
            ->limit($limit)
            ->get();
    }

    /**
     * @param Request $request
     * @param string $id
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function update(Request $request, $id = '')
    {
        $form = $request->post();

        if (empty($id)) {
            throw new \Exception('No person id!');
        } elseif (empty($form)) {
            throw new \Exception('No input data!');
        }

        if ($exist = Person::query()->find($id)) {
            $exist->fill($form);
            return response()
                ->json([
                    'updated' => $exist->save(),
                    'id' => $exist->getAttribute('id'),
                ]);
        }

        throw new \Exception('Person is not exists!');
    }

    /**
     * @param string $id
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function delete($id = '')
    {
        if (empty($id)) {
            throw new \Exception('No person id!');
        }

        if ($exist = Person::query()->find($id)) {
            return response()
                ->json([
                    'deleted' => $exist->delete(),
                ]);
        }

        throw new \Exception('Person is not exists!');
    }

    /**
     * @param $id
     * @return \Illuminate\Database\Eloquent\Builder[]|\Illuminate\Database\Eloquent\Collection
     */
     public function showOne($id)
    {
        return Person::query()
            ->where("id",$id)
            ->get();
    }
}

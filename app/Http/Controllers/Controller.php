<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Laravel\Lumen\Routing\Controller as BaseController;

class Controller extends BaseController
{
    /**
     * @param $data
     * @param array $rules
     * @param array $messages
     * @param array $customAttributes
     * @return bool|void
     */
    public function validateNew($data, array $rules, array $messages = [], array $customAttributes = [])
    {
        if ($data instanceof Request) {
            $data = $data->all();
        }
        $validator = $this->getValidationFactory()->make($data, $rules, $messages, $customAttributes);

        if ($validator->fails()) {
            return $this->sendError(400, $validator->errors());
        }

        return true;
    }

    /**
     * @param $data
     * @param int $status
     * @param array $headers
     * @return \Illuminate\Http\JsonResponse
     */
    public function sendResponse($data, $status = 200, $headers = [])
    {
        $response = [
            'data' => $data,
            'status' => $status === 200 ? 1 : 0,
        ];
        return response()->json($response, $status, $headers, JSON_PRETTY_PRINT);
    }

    /**
     * @param $response
     * @param int $status
     * @param array $headers
     * @return \Illuminate\Http\JsonResponse
     */
    public function sendListResponse($response, $status = 200, $headers = [])
    {
        $response['status'] = 1;
        return response()->json($response, $status, $headers, JSON_PRETTY_PRINT);
    }

    /**
     * @param $status
     * @param $errors
     */
    public function sendError($status, $errors)
    {
        $response = [
            'status' => 0,
            'errors' => $errors
        ];
        $headers = [
            'Access-Control-Allow-Origin' => '*',
            'Access-Control-Allow-Methods' => 'POST, GET, OPTIONS, PUT, DELETE',
            'Access-Control-Allow-Headers' => 'Content-Type, X-Auth-Token, Origin, x-access-token, Access-Control-Request-Method, Access-Control-Request-Headers, Access-Control-Allow-Headers',
            'Access-Control-Allow-Credentials' => 'true'
        ];
        response()->json($response, $status, $headers, JSON_PRETTY_PRINT)->send();
        die;
    }

    /**
     * @param $request
     * @param $entity
     * @param $entity_id
     * @param int $edit in this action user need edit permissions
     * @param int $delete in this action user need delete permissions
     * @param int $access in this action user need access permissions to give permissions to other users
     * @return bool
     */
    public function hasAccess($request, $entity, $entity_id, $edit = 0, $delete = 0, $access = 0)
    {
        if (isset($request->permission["system"])) {
            return true;
        }
        if (isset($request->permission[$entity][$entity_id]) && $request->permission[$entity][$entity_id]["view"] == 1) {
            if ((($request->permission[$entity][$entity_id]["edit"] == $edit
                        || $request->permission[$entity][$entity_id]["edit"] == 1)
                    && ($request->permission[$entity][$entity_id]["delete"] == $delete
                        || $request->permission[$entity][$entity_id]["delete"] == 1)
                    && ($request->permission[$entity][$entity_id]["access"] == $access
                        || $request->permission[$entity][$entity_id]["access"] == 1))
                || $request->permission[$entity][$entity_id]["full"]) {
                return true;
            }
        }
        return false;
    }
}

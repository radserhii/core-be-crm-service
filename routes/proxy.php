<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/
/**
 * @var \Laravel\Lumen\Routing\Router $router
 */

$router->group(['prefix' => 'proxy', 'middleware' => ['auth']], function () use ($router) {
    $router->get('forms', 'Proxy\FormBuilderProxyController@get');
    $router->get('forms/{id}', 'Proxy\FormBuilderProxyController@getById');
    $router->post('forms', 'Proxy\FormBuilderProxyController@create');
    $router->put('forms/{id}', 'Proxy\FormBuilderProxyController@update');
    $router->delete('forms/{id}', 'Proxy\FormBuilderProxyController@delete');
    $router->delete('forms', 'Proxy\FormBuilderProxyController@bulkSoftdelete');
    $router->post('forms/status', 'Proxy\FormBuilderProxyController@bulkSetStatus');

    $router->get('schema', 'Proxy\FormBuilderProxyController@schema');
    $router->get('mapped', 'Proxy\FormBuilderProxyController@mapped');

    $router->get('forms/person/{id}', 'Proxy\FormBuilderProxyController@getFormsByPersonId');
});

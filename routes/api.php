<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/
/**
 * @var \Laravel\Lumen\Routing\Router $router
 */

$router->get('/', function () use ($router) {
    //return $router->app->version();
    $response = [
        'status' => 1,
        'data' => "Laravel 5.5.* Lumen 5.5.0 RESTful API with OAuth2"
    ];

    return response()->json($response, 200, [], JSON_PRETTY_PRINT);
});
$router->post('login', 'UserController@login');

$router->group(['middleware' => ['auth']], function () use ($router) {

//  Person
    $router->put('persons', 'PersonController@create');
    $router->get('persons/schema', function () {
        return (new \App\Models\Person)->schema();
    });
    $router->get('persons', 'PersonController@search');
    $router->get('persons/{id}', 'PersonController@showOne');
    $router->post('persons/{id}', 'PersonController@update');
    $router->delete('persons/{id}', 'PersonController@delete');

//  User
    $router->get('logout', 'UserController@logout');
    $router->get('me', 'UserController@me');
    $router->post('refresh', 'UserController@refresh');


});